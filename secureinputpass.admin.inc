<?php
/**
 * @file
 * Definition of configuration forms.
 */

/**
 * Form builder function for the general secure input pass configuration.
 */
function secureinputpass_admin_form($form, &$form_state) {

  $options = array(
    SECUREINPUTPASS_BEHAVIOR_ALL => t('Add security attributes for password fields in all forms.'),
    SECUREINPUTPASS_BEHAVIOR_SPECIFIC => t('Add security attributes for password fields in specific forms.'),
    SECUREINPUTPASS_BEHAVIOR_NONE => t('Do not alter any form.'),
  );

  $form['secureinputpass_behavior'] = array(
    '#title' => t('Secure input pass behavior'),
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => variable_get('secureinputpass_behavior', SECUREINPUTPASS_BEHAVIOR_ALL),
    '#description' => t("Defines the behavior for adding security attributes to password fields."),
  );
  $form['secureinputpass_specific_form'] = array(
    '#title' => t('Specific forms'),
    '#type' => 'textarea',
    '#default_value' => variable_get('secureinputpass_specific_form', ''),
    '#description' => t("Id forms to apply. Separate multiple values using comma (,)."),
  );

  return system_settings_form($form);
}
